package wired2perform;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.TestApp;

import java.time.Duration;

public class LoginSuccessPage {

    @FindBy(xpath = "//*[contains(text(),'Create Post')]")
    private WebElement salutationMessageElement;

    public String getSalutationMessage(){
        TestApp.getInstance().waitUntilNextElementAppears(By.xpath("//*[contains(text(),'Create Post')]"),
                Duration.ofSeconds(10));
        return salutationMessageElement.getText();
    }
}
