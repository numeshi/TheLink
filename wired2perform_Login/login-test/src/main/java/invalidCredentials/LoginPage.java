package invalidCredentials;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.TestApp;
import wired2perform.LoginSuccessPage;

import java.time.Duration;

public class LoginPage {
    @FindBy(id = "username")
    private WebElement mailElement;

    @FindBy(id = "password")
    private WebElement passwordElement;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement submitButtonElement;

    public LoginPage setEmail(String email){
        TestApp.getInstance().waitUntilNextElementAppears(By.id("username"),
                Duration.ofSeconds(10));
        mailElement.sendKeys(email);
        return this;
    }

    public LoginPage setPassword(String password){
        TestApp.getInstance().waitUntilNextElementAppears(By.id("password"),
                Duration.ofSeconds(10));
        passwordElement.sendKeys(password);
        return this;
    }

    public LoginUnSuccessPage submit(){
        TestApp.getInstance().waitUntilNextElementAppears(By.xpath("//button[@type='submit']"),
                Duration.ofSeconds(10));
        submitButtonElement.click();
        return PageFactory.initElements(TestApp.getInstance().getDriver(),
                LoginUnSuccessPage.class);
    }

}
