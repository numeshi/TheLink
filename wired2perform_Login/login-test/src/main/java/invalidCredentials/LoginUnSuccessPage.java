package invalidCredentials;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.TestApp;

import java.time.Duration;

public class LoginUnSuccessPage {
    @FindBy(className = "error-reason")
    private WebElement errorMessageElement;

    public String getErrorMessage(){
        TestApp.getInstance().waitUntilNextElementAppears(By.cssSelector("div.error-reason"),
                        //("/html/body/w2p-main/block-ui/div/div/w2p-login-home/div[1]/div/div[3]/div[1]/div[4]/form/div[4]"),
                Duration.ofSeconds(10));
        return errorMessageElement.getText();
    }
}
