package invalidCredentials;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.*;
import utils.TestApp;

public class LoginTest1 {

    WebDriver driver;
    invalidCredentials.LoginPage loginPage;
    invalidCredentials.LoginUnSuccessPage loginUnSuccessPage;

    @BeforeMethod
    public void setUp() {

        TestApp.getInstance().openBrowser();
        TestApp.getInstance().navigateToURL();

        loginPage = PageFactory.initElements(TestApp.getInstance().getDriver(),
                LoginPage.class);

    }

    @Test
    public void testUnSuccessLogin() throws InterruptedException {
        loginUnSuccessPage= loginPage
                .setEmail("harsha+1200@wired2perform.com")
                .setPassword("harsha@124")
                .submit();

        Thread.sleep(3000);

//        Assert.assertEquals(loginUnSuccessPage.getErrorMessage(),
//                "Don’t have an Account? ",
//                "Test failed");


    }

    @Test
    public void testUnSuccessLogin1() throws InterruptedException {
        loginUnSuccessPage= loginPage
                .setEmail("harshad+1200@wired2perform.com")
                .setPassword("harsha@123")
                .submit();

        Thread.sleep(3000);
    }

    @Test
    public void testUnSuccessLogin2() throws InterruptedException {
        loginUnSuccessPage= loginPage
                .setEmail("harshad+1200@wired2perform.com")
                .setPassword("harsha@126")
                .submit();

        Thread.sleep(3000);
    }

    @Test
    public void testUnSuccessLogin3() throws InterruptedException {
        loginUnSuccessPage= loginPage.submit();

        Thread.sleep(3000);

    }

    @AfterMethod
    void afterMethod(){
        if (driver != null) {
            driver.quit();
    }
    }

    @AfterSuite
    void afterSuite(){
        if (driver != null) {
            driver.close();
        }
    }
}
