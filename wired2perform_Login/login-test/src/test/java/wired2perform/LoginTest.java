//TC_Wired2Perform_Dev_Application_Login_01

package wired2perform;

import invalidCredentials.LoginUnSuccessPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import utils.TestApp;

public class LoginTest {

    LoginPage loginPage;
    LoginSuccessPage loginSuccessPage;


    @BeforeMethod
    public void setUp() {

        TestApp.getInstance().openBrowser();
        TestApp.getInstance().navigateToURL();

        loginPage = PageFactory.initElements(TestApp.getInstance().getDriver(),
                LoginPage.class);

    }

    @Test
    public void testLogin() {
        loginSuccessPage=loginPage
                .setEmail("harsha+1200@wired2perform.com")
                .setPassword("harsha@123")
                .submit();

        Assert.assertEquals(loginSuccessPage.getSalutationMessage(),
                "Create Post",
                "Failed to login");

//        Assert.assertEquals(loginSuccessPage.getSalutationMessage(),
//                "How are you feeling today?",
//                "Failed to login");

    }


}
