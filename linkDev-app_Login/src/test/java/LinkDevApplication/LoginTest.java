package LinkDevApplication;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.time.Duration;

public class LoginTest {
    WebDriver driver;

    @DataProvider(name= "data-set")
    public static Object[][] DataSet(){
        Object[][] obj={
                {"valid","harsha+1200@wired2perform.com","harsha@123"},
                {"invalid","harsha+1200@wired2perform.com","harsha@124"},
                {"invalid","harshad+1200@wired2perform.com","harsha@123"},
                {"invalid","harshad+1200@wired2perform.com","harsha@126"},
                {"empty","",""}
        };
        return obj;
    }

    @Test(dataProvider = "data-set")
    public void DataProvTest(String type,String username,String password ) throws InterruptedException {

        System.setProperty("webdriver.chrome.driver","resources/chromedriver.exe");
        System.out.println(type+ " " +username+ " " +password);
        driver=new ChromeDriver();
        WebDriverWait wait=new WebDriverWait(driver, Duration.ofSeconds(10));

        driver.get("https://dev.thelink.zone/start");

        driver.findElement(By.cssSelector("input[name='email']")).sendKeys(username);
        driver.findElement(By.cssSelector("input[name='password']")).sendKeys(password);
        driver.findElement(By.xpath("//*[contains(text(),'LOGIN')]")).click();

        if(type.equals("valid")){
            wait.until(ExpectedConditions.presenceOfElementLocated(
                    By.cssSelector("[class='link-logo-style']")));
            Thread.sleep(3000);
        } else if(type.equals("invalid")){
            wait.until(ExpectedConditions.presenceOfElementLocated(
                    By.cssSelector("[class='error-common ios hydrated']")));
        }else{
            wait.until(ExpectedConditions.presenceOfElementLocated(
                    By.cssSelector("[class='error']")));
        }
        Thread.sleep(3000);
        driver.quit();

    }

    @Test
    public void test() {

    }
}
