package LinkDevApplication;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ForgotPasswordTest {
    WebDriver driver;

    @BeforeMethod
    public void setUp() throws InterruptedException {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("https://dev.thelink.zone/start");
        driver.manage().window().maximize();

       // driver.findElement(By.linkText("Forgot password?")).click();
        driver.findElement(By.xpath("//*[@id=\"main-content\"]/app-link-start/ion-content/div[2]/div/div/ion-grid/ion-row/ion-col[2]/form/ion-grid/div/ion-row[5]/ion-col/ion-button")).click();
        Thread.sleep(3000);
    }

    @Test
    public void testForgotPassword() {
        driver.quit();
    }

}
